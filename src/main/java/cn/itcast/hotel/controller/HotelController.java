package cn.itcast.hotel.controller;

import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import cn.itcast.hotel.service.IHotelService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/hotel")
public class HotelController {

    @Autowired
    private IHotelService iHotelService;

    @PostMapping("/list")
    public PageResult serch(@RequestBody RequestParams requestParams) throws IOException {
        return iHotelService.serch(requestParams);}

//
//    @PostMapping("/filters")
//    public PageResult fitter(@RequestBody RequestParams requestParams) throws IOException {
//        return iHotelService.serch(requestParams);
//    }

}
