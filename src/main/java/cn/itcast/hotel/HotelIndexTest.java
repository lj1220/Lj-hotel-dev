package cn.itcast.hotel;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.mapper.Mapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static cn.itcast.hotel.constants.HotelConstants.MAPPING_TEMPLATE;
//索引类
public class HotelIndexTest {
    //创建client对象,这个对象里封装了关于索引库的操作方法,下面的测试类增删查会用到
    @Autowired
    private RestHighLevelClient client;

    //测试类中任何一个测试方法执行之"前"都先执行该注解标注的方法,也就是每次进行初始化
    @BeforeEach  //初始化代码
    void setUp(){
       this.client= new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://192.168.52.100:9200")
        ));
    }

    //测试类中任何一个测试方法执行之"后"都先执行该注解标注的方法,也就是每次都关闭
    @AfterEach
    void tearDown() throws IOException {
        this.client.close();
    }

    @Test//有这个注解,测试类就不用写主方法了
    //创建索引库
    void createHotelIndex() throws IOException {
        //创建request对象
        CreateIndexRequest request = new CreateIndexRequest("hotel");//索引库名字
        //请求参数,MAPPING_TEMPLATE是静态常量字符串,内容是创建索引库的dsl语句,json语句
        request.source(MAPPING_TEMPLATE, XContentType.JSON);
        //发送请求  client.indices方法包含了所有索引库操作的方法
        client.indices().create(request, RequestOptions.DEFAULT);
    }

    @Test
    //删除索引库
    void testDeleteHotelIndex() throws IOException {
        //创建request对象
        DeleteIndexRequest request = new DeleteIndexRequest("hotel");//索引库名字
        //发送请求
        client.indices().delete(request,RequestOptions.DEFAULT);
    }

    @Test
    //判断索引库是否存在
    void testExistsHotelIndex() throws IOException {
        //创建quest对象
        GetIndexRequest request = new GetIndexRequest("hotel");
        //发送请求
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        //判断是否存在
        System.out.println(exists ? "索引库已存在" : "索引库不存在");
    }


}
