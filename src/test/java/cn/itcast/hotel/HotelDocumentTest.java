package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.io.IOException;
import java.util.List;

//文档类

@Slf4j
@SpringBootTest
public class HotelDocumentTest {

    private RestHighLevelClient client;


    @Autowired
    private IHotelService service;
    //初始化
    @BeforeEach
    void setUp(){
        this.client = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://192.168.52.100:9200")
        ));
    }
    @AfterEach
    void tearDown() throws IOException {
        this.client.close();
    }



/*    POST /{索引库名}/_doc/1
    {
        "name": "Jack",
            "age": 21
    }*/

    //新增文档
    @Test
    void testAddDocument() throws IOException {
        // 1.根据id查询酒店数据

        Hotel hotel = service.getById(46829L); //里面填写要查询的id
        //把酒店id的数据放进封装类里
        HotelDoc hotelDoc = new HotelDoc(hotel);
        //将封装类转为JSON类型
        String s = JSON.toJSONString(hotel);
        //准备request对象
        IndexRequest request = new IndexRequest("hotel").id(hotelDoc.getId().toString());
        //准备JSON文档
        request.source(s,XContentType.JSON);
        //发送请求
        client.index(request, RequestOptions.DEFAULT);
    }

    //查询文档
    @Test
    void testGetDocumentById() throws IOException {
        //准备request对象
        GetRequest request = new GetRequest("hotel","46829");
        //发送请求
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        //解析响应结果
        String sourceAsString = response.getSourceAsString();
        //输出结果
        HotelDoc hotelDoc = JSON.parseObject(sourceAsString,HotelDoc.class);
        System.out.println(hotelDoc);
    }

    //删除文档  只需要准备request 和 发送请求
    @Test
    void testDeleteDocument() throws IOException {
        //准备request请求
        DeleteRequest request = new DeleteRequest("hotel","46829");
        //发送请求
        DeleteResponse response = client.delete(request,RequestOptions.DEFAULT);

    }

    //修改文档
    @Test
    void testUpdateDocument() throws IOException {
        //准备request请求
        UpdateRequest request = new UpdateRequest("hotel","46829");
        //准备请求参数
        request.doc(
                "name","城市便捷酒店",
                "address","珠海"
        );
        //发送请求
        UpdateResponse response = client.update(request,RequestOptions.DEFAULT);
    }

    //批量导入文档
    @Test
    void testBulkRequest() throws IOException {
        //批量查询酒店数据
        List<Hotel> hotels = service.list();
        //准备request请求
        BulkRequest request = new BulkRequest();
        //准备参数添加多个新增的Request
        for (Hotel hotel : hotels) {
            //转化为文档的类型的实体类
            HotelDoc hotelDoc = new HotelDoc(hotel);
            //创建新增文档的request对象
            request.add(new IndexRequest("hotel").id(hotelDoc.getId().toString()).source(JSON.toJSONString(hotelDoc),XContentType.JSON));
        }
        //发送请求
        client.bulk(request,RequestOptions.DEFAULT);
    }


    @Test
    void testmatchall() throws IOException {
//        准备request
        SearchRequest request = new SearchRequest("hotel");

//        准备dsl
        request.source().query(QueryBuilders.matchAllQuery());

//        发送请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);

//        解析响应
        aaa(response);
    }



    private void aaa(SearchResponse response) {
//        解析响应
        SearchHits searchHits = response.getHits();

        long total = searchHits.getTotalHits().value;

        System.out.println("共搜索到" + total + "条数据");
        // 4.2.文档数组
        SearchHit[] hits = searchHits.getHits();
        // 4.3.遍历
        for (SearchHit hit : hits) {
            // 获取文档source
            String json = hit.getSourceAsString();
            // 反序列化
            HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
            System.out.println("hotelDoc = " + hotelDoc);
            
        }

    }

}
